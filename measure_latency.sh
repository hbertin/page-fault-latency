#!/bin/bash

if [ $# -ne 2 ]; then
    echo "Invalid arguments number: arg1: file, arg2: number of measurements repetitions"
    exit 0
fi

for i in $(seq 0 $2); 
do
    sync; echo 3 > /proc/sys/vm/drop_caches
    echo $i
    ./page-fault-latency $1 >> results_nocount.txt
    sleep 7
done