import matplotlib.pyplot as plt
import re
import statistics as stats

file_path = 'results.txt'

with open(file_path, 'r') as file:
    latencies = []

    for line in file:
        latency = re.split(r'latency:\s+(\d+)\s+ns', line)[1]
        latencies.append(int(latency)/1000000)

    print(latencies)

    # Generate a box plot
    plt.boxplot(latencies)
    plt.title('Latencies handling MajPFs (N=100)')
    plt.ylabel('Latency (ms)')
    plt.savefig('majPFs-latencies.png')

    # get some stats 
    print('min(ms):    ', min(latencies))
    print('max(ms):    ', max(latencies))
    print('mean(ms):   ', stats.mean(latencies))
    print('median(ms): ', stats.median(latencies))
    print('stdev(ms):  ', stats.stdev(latencies))