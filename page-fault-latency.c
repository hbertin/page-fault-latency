#include <fcntl.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/syscall.h>         /* Definition of SYS_* constants */
#include <sys/ioctl.h>
#include <linux/perf_event.h>    /* Definition of PERF_* constants */
#include <linux/hw_breakpoint.h> /* Definition of HW_* constants */
#include <unistd.h>
#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

static long perf_event_open(struct perf_event_attr *hw_event,
                            pid_t pid,
                            int cpu,
                            int group_fd,
                            unsigned long flags)
{
    int ret = syscall(__NR_perf_event_open, hw_event, pid, cpu,
                      group_fd, flags);
    return ret;
}

static inline uint64_t getns(void)
{
    struct timespec ts;
    int ret = clock_gettime(CLOCK_REALTIME, &ts);
    if(ret != 0){
        printf("Unvalid time, ret: %d\n", ret);
        exit(1);
    }
    return (((uint64_t)ts.tv_sec) * 1000000000ULL) + ts.tv_nsec;
}

static long get_page_size(void)
{
    long ret = sysconf(_SC_PAGESIZE);
    if (ret == -1) {
        perror("sysconf/pagesize");
        exit(1);
    }
    return ret;
}


int main(int argc, char **argv)
{   
    // Getting info about the file we want to map
    int fd = open(argv[1], O_RDONLY);
    if(fd < 0){
        printf("\n\"%s \" could not open\n", argv[1]);
        exit(1);
    }

    // get file stats
    struct stat stats;
    int err = fstat(fd, &stats);
    if(err < 0){
        printf("\n\"%s \" could not open\n", argv[1]);
        exit(2);
    }
    // printf("File size: %ld B\n", stats.st_size);

    // Mapping the file
    char *map = (char *)mmap(NULL, stats.st_size, PROT_READ, MAP_SHARED, fd, 0);
    if (map == MAP_FAILED)
    {
        perror("Failed to mmap");
        return 1;
    }

    // Uncomment if yout want to verify the number of MajPFs triggered
    // configure perf event for counting Major Page Faults
    // struct perf_event_attr pe_attr_page_faults;
    // memset(&pe_attr_page_faults, 0, sizeof(pe_attr_page_faults));
    // pe_attr_page_faults.size = sizeof(pe_attr_page_faults);
    // pe_attr_page_faults.type =   PERF_TYPE_SOFTWARE;
    // pe_attr_page_faults.config = PERF_COUNT_SW_PAGE_FAULTS_MAJ;
    // pe_attr_page_faults.disabled = 1;
    // pe_attr_page_faults.exclude_kernel = 0;
    // int page_faults_fd = perf_event_open(&pe_attr_page_faults, 0, -1, -1, 0);
    // if (page_faults_fd == -1) {
    //     printf("perf_event_open failed for page faults: %s\n", strerror(errno));
    //     return -1;
    // }

    // get page size
    long page_size = get_page_size();
    long pages_number = (stats.st_size/page_size)+1;
    // printf("Number of pages: %ld\n", pages_number);

    uint64_t latency[pages_number];
    uint64_t start;
    uint64_t stop;
    uint64_t page_faults_count;

    // track the latencies for each page
    uint64_t *latencies = malloc(sizeof(uint64_t) * pages_number);
    memset(latencies, 0, sizeof(uint64_t) * pages_number);

    // // Start counting major page faults
    // ioctl(page_faults_fd, PERF_EVENT_IOC_RESET, 0);
    // ioctl(page_faults_fd, PERF_EVENT_IOC_ENABLE, 0);

    char *cur = map+stats.st_size;

    for (int i = 0; i < pages_number; i++)
    {
        // // Start counting major page faults
        // ioctl(page_faults_fd, PERF_EVENT_IOC_RESET, 0);
        // ioctl(page_faults_fd, PERF_EVENT_IOC_ENABLE, 0);

        start = getns();
        int v = *((int*)cur);
        stop = getns();

        // // Stop counting major page faults and read value
        // ioctl(page_faults_fd, PERF_EVENT_IOC_DISABLE, 0);
        // read(page_faults_fd, &page_faults_count, sizeof(page_faults_count));

        latencies[i] = stop-start;
        printf("%d: MajPFs: %ld, latency: %ld ns\n", i, page_faults_count, latencies[i]);

        cur -= page_size;
    }
    
    // unmap
    munmap(map, stats.st_size);
    if(err != 0){
        printf("UnMapping Failed\n");
        return 1;
    }

    return 0;
}