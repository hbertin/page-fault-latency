# Page Fault Latency

This project provide a script to measure the latency needed to handle a major page fault. 

## Compilation
```
gcc -o page-fault-latency page-fault-latency.c
```
## Run
```
./measure_latency.sh file2use numberOfMeasurements
```

## Results
- results are stored in results.txt
- you can plot the results thanks to the python script, the plot can be found under majPFs-latencies.png

```
python plot-results.py
```